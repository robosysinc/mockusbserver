import java.net.InetSocketAddress;
import java.nio.ByteBuffer;

import org.java_websocket.WebSocket;
import org.java_websocket.handshake.ClientHandshake;
import org.java_websocket.server.WebSocketServer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class SimpleServer extends WebSocketServer {

	private static WebSocket connection;
	
	public SimpleServer(InetSocketAddress address) {
		super(address);
	}

	@Override
	public void onOpen(WebSocket conn, ClientHandshake handshake) {
		connection = conn;
		connection.send("Client " + conn.getRemoteSocketAddress() + " connected to broker"); //This method sends a message to the new client
		//broadcast( "new connection: " + handshake.getResourceDescriptor() ); //This method sends a message to all clients connected
		System.out.println("new connection to " + conn.getRemoteSocketAddress());
	}
	
	private static void addProduct(String upc) {
		connection.send("BQ=" + upc + "\r" + "MD=4\r");
	}
	
	private static void removeProduct(String upc) {
		connection.send("BQ=" + upc + "\r" + "MD=3\r");
	}

	@Override
	public void onClose(WebSocket conn, int code, String reason, boolean remote) {
		System.out.println("closed " + conn.getRemoteSocketAddress() + " with exit code " + code + " additional info: " + reason);
	}

	@Override
	public void onMessage(WebSocket conn, String message) {
		System.out.println("received message from "	+ conn.getRemoteSocketAddress() + ": " + message);
	}

	@Override
	public void onMessage( WebSocket conn, ByteBuffer message ) {
		System.out.println("received ByteBuffer from "	+ conn.getRemoteSocketAddress());
	}

	@Override
	public void onError(WebSocket conn, Exception ex) {
		System.err.println("an error occured on connection " + conn.getRemoteSocketAddress()  + ":" + ex);
	}
	
	@Override
	public void onStart() {
		System.out.println("server started successfully");
	}


	public static void main(String[] args) {
		// Setup GUI
		JFrame frame = new JFrame("USB Service");
	    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    frame.setSize(300, 300);
	    frame.getContentPane().setLayout(new FlowLayout());
	    
	    JButton addButton = new JButton("Add Product");
	    addButton.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		addProduct("012345678905");
	    	}
	    });
	    frame.add(addButton); 
	    
	    JButton removeButton = new JButton("Remove Product");
	    removeButton.addActionListener(new ActionListener() {
	    	public void actionPerformed(ActionEvent e) {
	    		removeProduct("012345678905");
	    	}
	    });
	    frame.add(removeButton);
	    frame.setVisible(true);
		
		// Setup Server
		String host = "192.168.1.31";
		int port = 8887;
			
		WebSocketServer server = new SimpleServer(new InetSocketAddress(host, port));
		server.run();
	}
}